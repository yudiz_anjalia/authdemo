//Signup--

window.onload = function() {
  //console.log(window.localStorage.getItem('users'))
  // window.location.href = "http://127.0.0.1:5500/signin.html"
};

function register() { 
  var email = document.getElementsByName("email")[0].value;
  
  let userDetails = JSON.parse(window.localStorage.getItem('users'));

  if(userDetails){ 
      var isUserExists = userDetails.filter(v=>v.email ==email);
      if(isUserExists.length){ 
          alert("Your account is already registered...");
          return false;
      }    
  }
  if(validation() == true){                 
      var first_name = document.getElementsByName("first_name")[0].value;
      var last_name = document.getElementById("last_name").value;
      var mobile = document.getElementById("mobile").value;
      var dob =  document.getElementById("dob").value; 
      var email = document.getElementsByName("email")[0].value;
      var password1 = document.getElementById("password1").value;
      var password2 = document.getElementById("cpassword").value;
      var gender = document.querySelector('input[name = "gender"]:checked').value; 

      const obj  = {first_name:first_name,dob:dob,last_name:last_name,email:email,mobile:mobile,password:password1,gender:gender,isLoggedIn:false};

      userDetails = (userDetails)?userDetails:[];
      userDetails.push(obj);
      window.localStorage.setItem('users',JSON.stringify(userDetails));
      window.location.href = window.location.origin+"signin.html";
  }            
  
}

function phoneValidation() { 
  return false;
}
function validation(){ 
  
  var name = document.getElementsByName("first_name")[0].value;
  var lname = document.getElementById("last_name").value;
  var email = document.getElementsByName("email")[0].value;
  var mobile = document.getElementById("mobile").value;
  var password1 = document.getElementById("password1").value;
  var password2 = document.getElementById("cpassword").value;   
  var gender = document.querySelector('input[name = "gender"]:checked').value; 
  var dob =  document.getElementById("dob").value; 
  
  if(name==""){ 
      document.getElementById("name_error").innerHTML = "Please enter your first name";
      document.getElementById("first_name").focus();
      return false;

  }if(lname==""){ 
      document.getElementById("lname_error").innerHTML = "Please enter your first name";
      document.getElementById("last_name").focus();
      return false;
  }else if(email==""){ 
      document.getElementById("email_error").innerHTML = "please enter your email";
      document.getElementById("email").focus();
      return false;
  }else if(mobile == ""){ 
      document.getElementById("mobile_error").innerHTML = "please enter your mobile";
      document.getElementById("mobile").focus();
      return false;
  }else if(password1==" " || password1.length==0){ 
      document.getElementById("p_error").innerHTML = "please enter your password";
      document.getElementById("password1").focus();
      return false;
  }else if(password2==" " || password2.length==0){ 
      document.getElementById("cp_error").innerHTML = "please enter your password";
      document.getElementById("cpassword").focus();
      return false;
  }else if(!dob.length){ 
      document.getElementById("dob_error").innerHTML = "Please provide date of birth";
      document.getElementById("dob").focus();
      return false;
  }
  else if(password1 != password2){ 
      document.getElementById("cp_error").innerHTML = "password did not match";
      return false;
  }else if(gender==" "){ 
      document.getElementById("gender_error").innerHTML = "Please select gender";
  }          
  else{ 
      return true;
  }
  
  
}

//sign in ---

function login(){
  if(validation() == true){
      var email = document.getElementsByName("email")[0].value;
      var password  =document.getElementsByName("password")[0].value;

      const userDetails = JSON.parse( window.localStorage.getItem('users') );

      if(userDetails){ 
          var isUserExists = userDetails.filter(v=>v.email ==email && v.password==password);
              isUserExists[0].isLoggedIn = true;
          if(isUserExists.length){                        
              
              window.localStorage.setItem('token',JSON.stringify(isUserExists[0]));
              alert("Logged In Successgully.");
              location.href= "dashboard.html"
          }else{
              alert("Incorrect Credentials.")
          }
      }else{
          alert("Incorrect Credentials.")
       }

      

      
  }        
}   
function validation() { 
  var email = document.getElementsByName("email")[0].value;
  var password  =document.getElementsByName("password")[0].value; 
  if(email==""){ 
      document.getElementById("email_error").innerHTML = "Please enter your eail or mobile";
      return false;
  }
  if(password==""){ 
      document.getElementById("password_error").innerHTML = "Please enter your password";
      return false;
  }
  return true;
}
function checkvalue(id){ 
  document.getElementById(id).innerHTML = "";
}
//time
let clock = document.getElementById("clock");


//todo
const form = document.querySelector('form');
let list = document.querySelector('.list');
let id = 1;
if(JSON.parse(localStorage.getItem("todoItems")) === null || JSON.parse(localStorage.getItem("todoItems")).length == 0){
  id = 1;
}else{
  id = JSON.parse(localStorage.getItem("todoItems"))[JSON.parse(localStorage.getItem("todoItems")).length -1].id+1 ;
  JSON.parse(localStorage.getItem("todoItems")).map(todo=>{

    let div = document.createElement('div');
    div.setAttribute("class","listItem");
    let input = document.createElement('input');
    input.setAttribute("type","checkbox");
    input.setAttribute("id",todo.id);
    div.append(input);
    let h1 = document.createElement('h1');
    h1.setAttribute("class","itemHeading");
    h1.innerHTML = todo.todo;
    div.append(h1);
    list.append(div);
  });
}
const inputs = document.querySelectorAll('input[type="checkbox"]');
let nonDItems = [];
for(let i =0; i<inputs.length; i++){
  inputs[i].addEventListener("click",e=>{
    JSON.parse(localStorage.getItem("todoItems")).map(todo=>{
      if(todo.id != e.target.id){
        nonDItems.push(todo);
      }
      localStorage.setItem("todoItems",JSON.stringify(nonDItems));
    });
      window.location.reload();
      alert('Done!');
  });
}
  //form submit
  
form.onsubmit = e=>{
  e.preventDefault();
  const value = e.target.name.value;
  if(value == ''){
    alert('Enter Something');
  }else{
    let items = [];
    let item = {
          id:id,
          todo:value
        };
    if(JSON.parse(localStorage.getItem("todoItems")) === null){
      items.push(item);
      localStorage.setItem('todoItems',JSON.stringify(items));
      window.location.reload();
      alert(' Added!');
    }else{
      JSON.parse(localStorage.getItem("todoItems")).map(todo=>{
        items.push(todo);
      });
      items.push(item);
      localStorage.setItem('todoItems',JSON.stringify(items));
      window.location.reload();
      alert('Added!');
    }
  }
}

  //time
  setInterval(function() {
      let date = new Date();
      clock.innerHTML = date.toLocaleTimeString(); 
  }, 1000);
  //gif
  const giphy = {
      baseURL: "https://api.giphy.com/v1/gifs/",
      apiKey: "0UTRbFtkMxAplrohufYco5IY74U8hOes",
      tag: "fail",
      type: "random",
      rating: "pg-13",
    };
    
    let giphyURL = encodeURI(
      giphy.baseURL +
        giphy.type +
        "?api_key=" +
        giphy.apiKey +
        "&tag=" +
        giphy.tag +
        "&rating=" +
        giphy.rating
    );
    setInterval(() => {
      getGif();
    }, 120000);
    function getGif() {
      fetch(giphyURL)
        .then((response) => {
          return response.json();
        })
        .then((gif) => {
          document.getElementById("gif-wrap").style.backgroundImage =
            'url("' + gif.data.images.original.url + '")';
        });
    }
    window.onloadeddata = setTimeout(getGif(), 2000);
    //logout
    function logout(){ 
      window.localStorage.removeItem('token'); 
      window.location.href = window.location.origin+"/sigin.html";       
  }